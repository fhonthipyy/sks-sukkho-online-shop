<?php
include('connection.php'); 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-------------------------------------------------- Title name ------------------------------------------->
    <title>SKS homepage</title>

    <!-------------------------------------------------- setting ------------------------------------------->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-------------------------------------------------- style ------------------------------------------->
    <link rel="stylesheet" href="_index.css">
    <script src="https://kit.fontawesome.com/yourcode.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>

<body>
    <!-- --------------------------------------------- navbar menu ------------------------------------------- -->
    <div class="nav-bar">
        <div class="nav-bar-left">
            <div class="logo">
                <a href="index.php">
                    <img class="logo-shop" src="./img/logo.png" alt="logo">
                </a>
            </div>
            <div class="nav-menu">
                <a href="./_aboutus.php" class="btn-abt-us">About us</a>
                <!-- <a href="./_product.php" class="btn-prd">Product</a> -->
                <a href="#" class="btn-cont">Contact</a>
            </div>
        </div>
           
        <div class="nav-bar-right">
            <div class="d-flex justify-content-end align-items-center" style="height:30px;">
			<!-- Button trigger modal -->
				<button type="button" class="btn btn-warnning " data-toggle="modal" data-target="#exampleModalCenter">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="black" width="20" class="bi bi-search"
                                viewBox="0 0 16 16">
                                <path
                                    d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85
                                    3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0
                                    1 1-11 0 5.5 5.5 0 0 1 11 0z" 
                                />
                    </svg>
				</button>
			</div>	
		
        
            <div class="text-end">
                <a href="_login-form.html" class="btn btn-outline-success">Log in</a>
                <a href="\SKS_project\register\register.html">
                    <button type="button" class="btn btn-outline-dark">
                        Sign-up
                    </button>
                </a>
            </div>

        </div>
		<!-- Modal -->
		<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Search Something..by products name</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
				<input type="text" class="form-control" id="search" placeholder="...">
				<hr>
				<div id="display"></div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>

 <script type="text/javascript">
     $(document).ready(function() {
	$("#search").keyup(function() { 
		var text = $('#search').val();
		if(text==""){
			$("#display").html(" ");
		}else{
			$.ajax({
				type: "POST",
				url: "search.php",
				data: {search: text},
				success: function(response) {
					$("#display").html(response);
				},
				error: function () { 
					$("#display").html("something wrong with ajax...!!");
				}
			});
		}
	});		
});	
 </script>


</div>      
    <!-- ------------------------------------------------ slide --------------------------------------------- -->
    <div class="slide">
        <div class="row justify-content-center" style="padding-right: 0px !important; " >
            <div class="col-8 show-left">
                <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="./img/1 Lanna Mask.svg" class="d-block w-100 h-80" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/3 Ganesh.svg " class="d-block w-100 h-80" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/img-GiantMask.svg" class="d-block w-100 h-80" alt="...">
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            <div class="col-4 show-right" style="font-size: 2rem;">
                <h1 style="font-size: 3rem;">Sukkho Thai Craft</h1>

                <h3 style="font-size: 1.5rem;padding: 0.5rem;">Design and make for all kinds of shows.
                    Masks and performance equipment.It is a handmade product every step of the way.
                    Each item represents the beautiful art and culture of Thailand. 
                   </h3>
            </div>
        </div>
    </div>
    <!-- ------------------------------------------------ content -------------------------------------------- -->
   
        <h1 class="head-content" style="padding: 5px;margin:1.5rem;">Our Products</h1>
        <div class="container">
        <div class="row row-content" style="padding-bottom: 5px;margin-top: 3.5rem;">            
         
            <a class="col-sm-4 card-menu" style="padding-bottom: 5px;  text-decoration: none;" href="./product/visit_lanna.php">
                <div class="card-body" style="width: 350px;height: auto;" >
                    <div class="img-menu">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="mask" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"
                            class="svg-inline--fa fa-mask fa-w-20 fa-7x">
                            <path fill="white"
                                d="M320.67 64c-442.6 0-357.57 384-158.46 384 39.9 0 77.47-20.69 101.42-55.86l25.73-37.79c15.66-22.99 46.97-22.99 62.63 0l25.73 37.79C401.66 427.31 439.23 448 479.13 448c189.86 0 290.63-384-158.46-384zM184 308.36c-41.06 0-67.76-25.66-80.08-41.05-5.23-6.53-5.23-16.09 0-22.63 12.32-15.4 39.01-41.05 80.08-41.05s67.76 25.66 80.08 41.05c5.23 6.53 5.23 16.09 0 22.63-12.32 15.4-39.02 41.05-80.08 41.05zm272 0c-41.06 0-67.76-25.66-80.08-41.05-5.23-6.53-5.23-16.09 0-22.63 12.32-15.4 39.01-41.05 80.08-41.05s67.76 25.66 80.08 41.05c5.23 6.53 5.23 16.09 0 22.63-12.32 15.4-39.02 41.05-80.08 41.05z">
                            </path>
                        </svg>
                    </div>
                    <h4 class="card-title">Lanna Mask</h4>
                    <p class="card-text">Handcraft lanna mask .</p>
                </div>
            </a>
        
            <a class="col-sm-4 card-menu" href="./product/visit_giant.php" style="padding-bottom: 5px;  text-decoration: none;">
                <div class="card-body" style="width: 350px;height: auto;">
                    <div class="img-menu">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="theater-masks" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"
                            class="svg-inline--fa fa-theater-masks fa-w-20 fa-7x">
                            <path fill="white"
                                d="M206.86 245.15c-35.88 10.45-59.95 41.2-57.53 74.1 11.4-12.72 28.81-23.7 49.9-30.92l7.63-43.18zM95.81 295L64.08 115.49c-.29-1.62.28-2.62.24-2.65 57.76-32.06 123.12-49.01 189.01-49.01 1.61 0 3.23.17 4.85.19 13.95-13.47 31.73-22.83 51.59-26 18.89-3.02 38.05-4.55 57.18-5.32-9.99-13.95-24.48-24.23-41.77-27C301.27 1.89 277.24 0 253.32 0 176.66 0 101.02 19.42 33.2 57.06 9.03 70.48-3.92 98.48 1.05 126.58l31.73 179.51c14.23 80.52 136.33 142.08 204.45 142.08 3.59 0 6.75-.46 10.01-.8-13.52-17.08-28.94-40.48-39.5-67.58-47.61-12.98-106.06-51.62-111.93-84.79zm97.55-137.46c-.73-4.12-2.23-7.87-4.07-11.4-8.25 8.91-20.67 15.75-35.32 18.32-14.65 2.58-28.67.4-39.48-5.17-.52 3.94-.64 7.98.09 12.1 3.84 21.7 24.58 36.19 46.34 32.37 21.75-3.82 36.28-24.52 32.44-46.22zM606.8 120.9c-88.98-49.38-191.43-67.41-291.98-51.35-27.31 4.36-49.08 26.26-54.04 54.36l-31.73 179.51c-15.39 87.05 95.28 196.27 158.31 207.35 63.03 11.09 204.47-53.79 219.86-140.84l31.73-179.51c4.97-28.11-7.98-56.11-32.15-69.52zm-273.24 96.8c3.84-21.7 24.58-36.19 46.34-32.36 21.76 3.83 36.28 24.52 32.45 46.22-.73 4.12-2.23 7.87-4.07 11.4-8.25-8.91-20.67-15.75-35.32-18.32-14.65-2.58-28.67-.4-39.48 5.17-.53-3.95-.65-7.99.08-12.11zm70.47 198.76c-55.68-9.79-93.52-59.27-89.04-112.9 20.6 25.54 56.21 46.17 99.49 53.78 43.28 7.61 83.82.37 111.93-16.6-14.18 51.94-66.71 85.51-122.38 75.72zm130.3-151.34c-8.25-8.91-20.68-15.75-35.33-18.32-14.65-2.58-28.67-.4-39.48 5.17-.52-3.94-.64-7.98.09-12.1 3.84-21.7 24.58-36.19 46.34-32.37 21.75 3.83 36.28 24.52 32.45 46.22-.73 4.13-2.23 7.88-4.07 11.4z">
                            </path>
                        </svg>
                    </div>
                    <h4 class="card-title">Giant Mask</h4>
                    <p class="card-text">Handcraft giant mask.</p>
                </div>
            </a>
            <a class="col-sm-4 card-menu"  href="./product/visit_special.php" style="padding-bottom: 5px;  text-decoration: none;">
                <div class="card-body" style="width: 350px;height: auto;">
                    <div class="img-menu">
                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="elephant" role="img"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"
                            class="svg-inline--fa fa-elephant fa-w-20 fa-7x">
                            <path fill="white"
                                d="M528 127.97c-8.84 0-16 7.16-16 16s7.16 16 16 16 16-7.16 16-16-7.16-16-16-16zm-16-96h-61.16c-3.64-3.77-7.46-7.4-11.71-10.66-25.97-19.88-59.44-26.22-91.82-17.46-18.04 4.9-33.88 14.96-46.49 28.11H192C85.96 31.97 0 117.93 0 223.98v112.01c0 8.84 7.16 16 16 16h16V480c0 17.67 14.33 32 32 32h80c17.67 0 32-14.33 32-32v-72.84c18.48 5.11 66.55 16.98 128 0V480c0 17.67 14.33 32 32 32h80c17.67 0 32-14.33 32-32V287.98h144v88.01c0 13.24-10.78 24-24 24s-24-10.77-24-24v-8c0-8.84-7.16-16-16-16h-16c-8.84 0-16 7.16-16 16v4.78c0 37.58 27.38 71.2 64.78 74.87 42.91 4.21 79.22-29.56 79.22-71.65V159.97c0-70.69-57.31-128-128-128zM400 464h-48V344.09c-120.67 33.34-111.08 31.2-224 0V464H80V303.98H48v-80.01c0-79.54 64.47-144.01 144-144.01h83.24c-6.11 26.93-2.43 54.18 11.54 77.47 11.53 19.19 28.91 34.05 49.22 42.53 0 40.15 27.18 73.73 64 84.26V464zm192-256.02c0 17.67-14.33 32-32 32H424c-22.06 0-40-17.94-40-40v-37.24c-22.65-4.59-41.89-6.4-56.06-30-16.43-27.46-7.38-71.86 31.94-82.57 29.16-7.91 55.52 6.33 66.66 29.8H512c44.11 0 80 35.89 80 80.01v48z">
                            </path>
                        </svg>
                    </div>
                    <h4 class="card-title">Special title treatment</h4>
                    <p class="card-text">Resin ganesh statue.</p>
                </div>
            </a>
        </div>
    </div>
        <!-- ------------------------------------------------- footer -------------------------------------------- -->
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="bootstrap" viewBox="0 0 118 94">
                <title>Bootstrap</title>
                <path fill-rule="white" clip-rule="evenodd"
                    d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0 44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51 94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624 10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5 0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0 5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772 10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281 0-5.406-3.903-8.178-11.425-8.178H49.948z">
                </path>
            </symbol>
            <symbol id="facebook" viewBox="0 0 16 16">
                <path
                    d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z" />
            </symbol>
            <symbol id="instagram" viewBox="0 0 16 16">
                <path
                    d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z" />
            </symbol>
            <symbol id="twitter" viewBox="0 0 16 16">
                <path
                    d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z" />
            </symbol>
        </svg>
         <!--------------------------------------- footer txt & icon ------------------------------ -->
        <div class="footer" style="margin-top: 5.5rem;">
            <div class="foot-text-copy">
                <p class="txt-footer mb-0">@copy;2021 Company, Inc</p>
            </div>
            <div class="icon">
                <div class="ms-2"><a class="text-muted" href="#"><svg class="bi" width="15" height="15" fill="white">
                            <use xlink:href="#twitter" />
                        </svg></a></div>
                <div class="ms-2"><a class="text-muted" href="#"><svg class="bi" width="15" height="15" fill="white">
                            <use xlink:href="#instagram" />
                        </svg></a></div>
                <div class="ms-2"><a class="text-muted" href="#"><svg class="bi" width="15" height="15" fill="white">
                            <use xlink:href="#facebook" />
                        </svg></a></div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="frontend-script.js"></script>
</body>

</html>
<?php  
    session_start();
    include("connection.php");   
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-------------------------------------------------- Title name ------------------------------------------->
    <title>SKS About Us</title>

    <!-------------------------------------------------- setting ------------------------------------------->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-------------------------------------------------- style ------------------------------------------->
    <link rel="stylesheet" href="_index.css">
    <script src="https://kit.fontawesome.com/yourcode.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<style>
    .dropbtn {
        background-color:  #04AA6D;
        color: white;
        margin-right: 0px;
        padding: 6px 10px;
        font-size: 20px;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        }

        .dropdown {
        position: relative;
        display: inline-block;
        background-color:transparent;
        }

        .dropdown-content {
        display: none;
        position: absolute;
        right: 0rem;
        background-color: #f9f9f9;
        min-width: 140px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        }

        .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        
        }

        .dropdown-content a:hover {background-color: #f1f1f1;}

        .dropdown:hover .dropdown-content {
        display: block;
        }

        .dropdown:hover .dropbtn {
        background-color:#A59193;
        color: black;
        }

        .w3-modal-content {
            width: 660px;
        }

        .text-center {
            text-align: center!important;
            background-color: #A59193;
            color: #fffff;10;
        }
        .w3-container, .w3-panel {
            padding: 0;
        }
        .w3-display-topright {
            position: absolute;
            right: 0;
            top: 0;
            color: white;
            z-index: 9;
        }
</style>

<body>
    <!-- --------------------------------------------- navbar menu ------------------------------------------- -->
    <div class="nav-bar">
        <div class="nav-bar-left">
            <div class="logo">
                <a href="_index_cus.php">
                    <img class="logo-shop" src="./img/logo.png" alt="logo">
                </a>
            </div>
            <div class="nav-menu">
                <a href="#" class="btn-abt-us disable" style="color:#380B10; font-weight:600;">About us</a>
                <!-- <a href="#" class="btn-prd">Product</a> -->
                <a href="#" class="btn-cont">Contact</a>
                <a href="./track.php" class="btn-prd">Track delivery</a>
            </div>
        </div>
        <div class="nav-bar-right">
        <div class="d-flex justify-content-end align-items-center" style="height:30px;">
			<!-- Button trigger modal -->
				<button type="button" class="btn btn-warnning " data-toggle="modal" data-target="#exampleModalCenter">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="black" width="20" class="bi bi-search"
                                viewBox="0 0 16 16">
                                <path
                                    d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85
                                    3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0
                                    1 1-11 0 5.5 5.5 0 0 1 11 0z" 
                                />
                    </svg>
				</button>
			</div>	
	<!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Search Something..by products name</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
				<input type="text" class="form-control" id="search" placeholder="...">
				<hr>
				<div id="display"></div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>

 <script type="text/javascript">
     $(document).ready(function() {
	$("#search").keyup(function() { 
		var text = $('#search').val();
		if(text==""){
			$("#display").html(" ");
		}else{
			$.ajax({
				type: "POST",
				url: "search.php",
				data: {search: text},
				success: function(response) {
					$("#display").html(response);
				},
				error: function () { 
					$("#display").html("something wrong with ajax...!!");
				}
			});
		}
	});		
});	
 </script>
            <div class="item-cart">
                <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="black" class="bi bi-cart-fill"
                        viewBox="0 0 16 16">
                        <path
                            d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                    </svg>
                </a>
            </div>

            <div class="text-end">
                
            <div class="dropdown " >
                <button class="dropbtn"><?php echo $_SESSION['uname'];?></button>
                <div class="dropdown-content">
                  
                    <a onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-white ">Profile</a>
                   
                    
                    <a class="w3-button w3-white" href="../_logout.php" onclick="return confirm('Are you sure you want to sign out?')" >
                         Sign out
                    </a>
                    
                </div>
            </div>

            </div>

        </div>

        </div>
    </div>
    <!-- ------------------------------------------------ slide --------------------------------------------- -->
    <!-- <div class="slide-content" style="color:#380B10;">
        <div class="row justify-content-center" style="padding-right: 0px !important; " >
            <div class="col-8 show-left">
                <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="./img/1 Lanna Mask.svg" class="d-block w-100 h-80" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/3 Ganesh.svg " class="d-block w-100 h-80" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/img-GiantMask.svg" class="d-block w-100 h-80" alt="...">
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            <div class="col-4 " style="font-size: 2rem;">
                <h1 style="font-size: 3rem;">Sukkho Thai Craft</h1>

                <h3 style="font-size: 1.5rem;padding: 0.5rem;">Design and make for all kinds of shows.
                    Masks and performance equipment.It is a handmade product every step of the way.
                    Each item represents the beautiful art and culture of Thailand. 
                   </h3>
            </div>
        </div>
    </div> -->
    <!-- ------------------------------------------------ content 01 -------------------------------------------- -->

   <div class="slide-content mt-5" align="center" style="color:#380B10;">
    <div class="col-6" style="font-size: 2rem;">
        <h1 style="font-size: 4rem;font-weight:800;">Sukkho Thai Craft Shop</h1>

        <h3 style="font-size: 1.5rem;padding: 1rem;">
            Sukkho shop has a business selling handicraft products. 
            It opened in 2015 as a small business. There is a location of the shop in Chiang Mai province,
            providing a service for selling sets. selling jewelry and selling handicrafts 
            The customers are Thai people and people who are involved in beauty contests, modeling and dancing.
        </h3>
    </div>
        <div class="row justify-content-center" style="padding-right: 0px !important;" >
            <div class="col-8" >
                <div id="carouselExampleControls" class="carousel" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="./img/ab-01.jpg" class="d-block w-50 h-40" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/ab-02.jpg " class="d-block w-50 h40" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/ab-03.jpg" class="d-block w-50 h-40" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/ab-04.jpg" class="d-block w-50 h-40" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/p01.jpg" class="d-block w-50 h-40" alt="...">
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            
        </div>
    </div>

    <!-- ------------------------------------------------ content 02 -------------------------------------------- -->
   <div class="slide-content mt-5" align="center" style="color:#380B10;">
    <div class="col-6" style="font-size: 2rem;">
        <!-- <h1 style="font-size: 3rem;">Sukkho Thai Craft</h1> -->

        <h3 style="font-size: 1.5rem;padding: 0.5rem;">
            design and manufacture Masks and display equipment
            Each show showcases the beautiful arts and culture of Thailand, Our main products for sale now are:
        </h3>
        <h3 style="font-size: 2rem;">1.Lanna Mask</h3>
    </div>
        <div class="row justify-content-center" style="padding-right: 0px !important;" >
            <div class="col-8" >
                <div id="carouselExampleControls" class="carousel" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="./img/img_mask04.jpg" class="d-block w-50 h-40" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/img_mask05.jpg " class="d-block w-50 h40" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/img_mask06.jpg" class="d-block w-50 h-40" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/img_mask08.jpg" class="d-block w-50 h-40" alt="...">
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            
        </div>
    </div>
    <!-- ------------------------------------------------ content 03 -------------------------------------------- -->
   <div class="slide-content mt-5" align="center" style="color:#380B10;">
    <div class="col-6" style="font-size: 2rem;">
        <!-- <h1 style="font-size: 3rem;">Sukkho Thai Craft</h1> -->

        <!-- <h3 style="font-size: 1.5rem;padding: 0.5rem;">
            design and manufacture Masks and display equipment
            Each show showcases the beautiful arts and culture of Thailand, Our main products for sale now are:
        </h3> -->
        <h3 style="font-size: 2rem;">2.Giant Mask</h3>
    </div>
        <div class="row justify-content-center" style="padding-right: 0px !important;" >
            <div class="col-8" >
                <div id="carouselExampleControls" class="carousel" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="./img/img_jMask_01.jpg" class="d-block w-50 h-40" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/img_jMask_02.jpg " class="d-block w-50 h40" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/img_jMask_05.jpg" class="d-block w-50 h-40" alt="...">
                        </div>                        
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            
        </div>
    </div>
     <!-- ------------------------------------------------ content 04 -------------------------------------------- -->
   <div class="slide-content mt-5" align="center" style="color:#380B10;">
    <div class="col-6" style="font-size: 2rem;">
        <!-- <h1 style="font-size: 3rem;">Sukkho Thai Craft</h1> -->

        <!-- <h3 style="font-size: 1.5rem;padding: 0.5rem;">
            design and manufacture Masks and display equipment
            Each show showcases the beautiful arts and culture of Thailand, Our main products for sale now are:
        </h3> -->
        <h3 style="font-size: 2rem;">3.Special title treatment</h3>
    </div>
        <div class="row justify-content-center" style="padding-right: 0px !important;" >
            <div class="col-8" >
                <div id="carouselExampleControls" class="carousel" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="./img/img_model_01.jpg" class="d-block w-50 h-40" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="./img/img_model_02.jpg " class="d-block w-50 h40" alt="...">
                        </div>
                                              
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            
        </div>
    </div>
    <!-- ------------------------------------------------ content 05 -------------------------------------------- -->
   <div class="slide-content mt-5" align="center" style="color:#380B10;">
    <div class="col-6" style="font-size: 2rem;">
        <h1 style="font-size: 3rem;">How to order!</h1>

        <!-- <h3 style="font-size: 1.5rem;padding: 0.5rem;">
            design and manufacture Masks and display equipment
            Each show showcases the beautiful arts and culture of Thailand, Our main products for sale now are:
        </h3> -->
        
    </div>
      
            <div class="col-8" >
                <div id="carouselExampleControls" class="carousel" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="./img/howto.png" class="d-block w-30 h-20" alt="...">
                        </div>
                       
                                              
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                        data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            
        </div>
    </div>
    
        <!-- ------------------------------------------------- footer -------------------------------------------- -->
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="bootstrap" viewBox="0 0 118 94">
                <title>Bootstrap</title>
                <path fill-rule="white" clip-rule="evenodd"
                    d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0 44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51 94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624 10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5 0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0 5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772 10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281 0-5.406-3.903-8.178-11.425-8.178H49.948z">
                </path>
            </symbol>
            <symbol id="facebook" viewBox="0 0 16 16">
                <path
                    d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z" />
            </symbol>
            <symbol id="instagram" viewBox="0 0 16 16">
                <path
                    d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z" />
            </symbol>
            <symbol id="twitter" viewBox="0 0 16 16">
                <path
                    d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z" />
            </symbol>
        </svg>
        <!--------------------------------------- footer txt & icon ------------------------------ -->
        <div class="footer" style="margin-top: 4rem;">
            <div class="foot-text-copy">
                <p class="txt-footer mb-0">@copy;2021 Company, Inc</p>
            </div>
            <div class="icon">
                <div class="ms-2"><a class="text-muted" href="#"><svg class="bi" width="15" height="15" fill="white">
                            <use xlink:href="#twitter" />
                        </svg></a></div>
                <div class="ms-2"><a class="text-muted" href="#"><svg class="bi" width="15" height="15" fill="white">
                            <use xlink:href="#instagram" />
                        </svg></a></div>
                <div class="ms-2"><a class="text-muted" href="#"><svg class="bi" width="15" height="15" fill="white">
                            <use xlink:href="#facebook" />
                        </svg></a></div>
            </div>
        </div>
</body>
<!-- ------------------ Modal Profile ----------------------------------->
<div id="id01" class="w3-modal">
        <div class="w3-modal-content">
        <div class="w3-container">
            <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
            <div class="">
          <div class="alert alert-header text-center" role="alert">
           <h2 style="color:white;">Profile</h2>
          </div>
        </div>
<div class="row d-flex justify-content-center">
    <div class='' style='width:30rem;  margin:0 0 50px 0; padding:0px;'>      
        <table width="600" border="0" align="center" class="square">
        <!-- <tr><td colspan="3" bgcolor="#CCCCCC" ><b class="d-flex justify-content-center" >Product  Detail</b></td></tr> -->
        
 <?php
       $sql = "SELECT* FROM users WHERE id= '$_SESSION[UserID]' ";  //เรียกข้อมูลมาแสดงทั้งหมด
       $result = mysqli_query($con, $sql);
       $row = mysqli_fetch_array($result);    
   ?>
   
   <!------------------------------------- profile detail ---------------------------->
   <tr class="mt-5">
   <!-- bgcolor="#EEEEEE" -->
       <td ><h5>Name</h5></td>
       <td><?php echo $row['fname'];?> &nbsp <?php echo $row['lname'];?> </td>
   </tr>
   <tr>
       <td width="22%"><h5>Address</h5></td>
       <td width="78%">
       
           <?php echo $row['addr'];?> &nbsp
           <?php echo $row['country'];?> &nbsp 
           <?php echo $row['zip'];?>
      
       </td>
   </tr>
   <tr>
         <td><h5>E-mail</h5></td>
         <td>
        <?php echo $row['email'];?>
       </td>
   </tr>
   <tr>
         <td ><h5>Phone</h5></td>
         <td><?php echo $row['phone'];?></td>
   </tr>
    
    
        </table>
      </div>
    </div>
        </div>
  </div>
</html>
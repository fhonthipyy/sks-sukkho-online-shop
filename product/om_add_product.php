
<?php
 session_start();
$db_host = "localhost";
$db_user = "root";
$db_password = "";
$db_name = "mydb";

try{
    $db = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    $e->getMessage();
}


if(isset($_REQUEST['btn_insert'])){
    try{
        $p_name=$_REQUEST['p_name']; 
        $p_cate=$_REQUEST['p_cate'];                                 
        $p_mdy=$_REQUEST['p_mdy'];
        $p_det=$_REQUEST['p_det'];
        $p_price=$_REQUEST['p_price'];
        $p_pdic=$_REQUEST['p_pdic'];
        $p_pmt=$_REQUEST['p_pmt'];

        $image_file = $_FILES['txt_file']['name'];
        $type = $_FILES['txt_file']['type'];
        $size = $_FILES['txt_file']['size'];
        $temp = $_FILES['txt_file']['tmp_name'];

        $p_qtt=$_REQUEST['p_qtt'];

        $path = "/product/upload/" . $image_file; //set upload folder path

        if (empty($p_name)){
            $errorMsg = "Please enter name" ;
         } else if (empty($image_file)){ 
             $errorMsg = "Please enter file" ;
         } else if ($type  == "image/jpg" || $type == 'image/jpeg' || $type == 'image/png' || $type == 'image/gif'){
             if (!file_exists($path)){
                 if ($size < 5000000){
                     move_uploaded_file($temp, 'upload/' .$image_file);
                 } else{
                     $errorMsg = "Your File too large pls upload 5MB size";
                 }
             }else{
                 $errorMsg = "File already Exists.. Check upload folder ";
             }
         } else{
             $errorMsg = "Upload JPG,JPEG,PNG, GIF";
         }

         if (!isset($errorMsg)){
             $insert_stmt = $db->prepare('INSERT INTO products (p_name,p_cate,p_mdy,p_det,p_price,p_pdic,p_pmt,p_img,p_qtt) VALUES (:fname, :fcate, :fmdy, :fdet, :fprice, :fpdic, :fpmt,:fimage, :fqtt)');
             $insert_stmt->bindParam(':fname', $p_name);
             $insert_stmt->bindParam(':fcate', $p_cate);
             $insert_stmt->bindParam(':fmdy', $p_mdy);
             $insert_stmt->bindParam(':fdet', $p_det);
             $insert_stmt->bindParam(':fprice', $p_price);
             $insert_stmt->bindParam(':fpdic', $p_pdic);
             $insert_stmt->bindParam(':fpmt', $p_pmt);
             $insert_stmt->bindParam(':fimage', $image_file);
             $insert_stmt->bindParam(':fqtt', $p_qtt);

             if ($insert_stmt->execute()){
                 $insertMsg = "File Upload Successfully....";
                 header('refresh:1;om_product.php');
             }
         }
    
    
     }catch(PDOException $e){
         $e->getMessage();
     }
 }
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>SKS Owner Page</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/dashboard/">

    <!-- Custom styles for this template -->
    <link href="om_product.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"></script>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

  header.card-header {
    background-color: #A59193;
    color:black;
    border-radius:10px 10px 0 0!important;

  }
  .card.card-form {
    border-radius: 10px;
    margin-top: 3rem;
}

.dropbtn {
        background-color: #380B10;
        color: white;
        margin-right: 3rem;
        padding: 6px 10px;
        font-size: 20px;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        }

        .dropdown {
        position: relative;
        display: inline-block;
        background-color:transparent;
        }

        .dropdown-content {
        display: none;
        position: absolute;
        right: 3rem;
        background-color: #f9f9f9;
        min-width: 140px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        }

        .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        
        }

        .dropdown-content a:hover {background-color: #f1f1f1;}

        .dropdown:hover .dropdown-content {
        display: block;
        }

        .dropdown:hover .dropbtn {
        background-color:#A59193;
        color: black;
        }

        .w3-modal-content {
            width: 660px;
        }

        
        .w3-container, .w3-panel {
            padding: 0;
        }
        .w3-display-topright {
            position: absolute;
            right: 0;
            top: 0;
            color: white;
            z-index: 9;
        }
    </style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

</head>

<body>

     <!-- --------------------------------------------- nav-bar------------------------------------------- -->

  <header class="navbar navbar-dark sticky-top bg-light flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" style="background-color: white;" href="./owner.php">
            <img class="logo-shop" style="width: 200px;" src="../img/logo.png" alt="logo">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                   
            <div class="dropdown " >
                <button class="dropbtn"><?php echo $_SESSION['uname'];?></button>
                <div class="dropdown-content">
                  
                    <a onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-white ">Profile</a>
                   
                    
                    <a class="w3-button w3-white" href="../_logout.php" onclick="return confirm('Are you sure you want to sign out?')" >
                         Sign out
                    </a>
                    
                </div>
            </div>

            </div>
        </div>
    </header>

    <div class="container-fluid " >
        <div class="row" >
          
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="../owner/owner.php">
                                <span data-feather="home">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-home" aria-hidden="true">
                                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                        <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                    </svg>
                                </span>
                                Owner Page
                            </a>
                        </li>
                        <li class="nav-item" >
                            <a class="nav-link " href="om_order.php" style="color: black;">
                                <span data-feather="file">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-file" aria-hidden="true">
                                        <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>
                                        <polyline points="13 2 13 9 20 9"></polyline>
                                    </svg>
                                </span>
                                Orders
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" aria-current="disable" href="">
                                <span data-feather="shopping-cart">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-shopping-cart"
                                        aria-hidden="true">
                                        <circle cx="9" cy="21" r="1"></circle>
                                        <circle cx="20" cy="21" r="1"></circle>
                                        <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6">
                                        </path>
                                    </svg>
                                </span>
                                Products
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="../staff/staff.php" style="color: black;">
                                <span data-feather="users">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-users" aria-hidden="true">
                                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="9" cy="7" r="4"></circle>
                                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                    </svg>
                                </span>
                                Staffs
                            </a>
                        </li>
                        <li class="nav-item " >
                            <a class="nav-link " href="../owner/cus_mng.php" style="color: black;">
                                <span data-feather="users">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-users" aria-hidden="true">
                                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="9" cy="7" r="4"></circle>
                                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                    </svg>
                                </span>
                                Customers
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" style="color: black;">
                                <span data-feather="bar-chart-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-bar-chart-2" aria-hidden="true">
                                        <line x1="18" y1="20" x2="18" y2="10"></line>
                                        <line x1="12" y1="20" x2="12" y2="4"></line>
                                        <line x1="6" y1="20" x2="6" y2="14"></line>
                                    </svg>
                                </span>
                                Reports
                            </a>
                        </li>
                    </ul>                    
                </div>
            </nav>
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="chartjs-size-monitor">
                    <div class="chartjs-size-monitor-expand">
                        <div class=""></div>
                    </div>
                    <div class="chartjs-size-monitor-shrink">
                        <div class=""></div>
                    </div>
                </div>
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Add new product</h1>
                </div>
            
                <!-- <p>A = Admin &nbsp;&nbsp;&nbsp; O = Owner </p> -->
                <!-------------------------------- form add new staff ----------------------------------->
                <div class="container" style="margin: 30px 0 0 30rem;">
                    <div class="row justify-content-center">
                          <!-- shhow error message -->
                            <?php 
                                if (isset($errorMsg)){ 
                            ?>
                                <div class="alert alert-danger text-center">
                                    <strong><?php echo $errorMsg; ?></strong>
                                </div>
                            <?php } ?>
                        <!-- shhow success message -->
                            <?php 
                                if (isset($insertMsg)){ 
                            ?>
                                <div class="alert alert-success text-center">
                                    <strong><?php echo $insertMsg; ?></strong>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-md-8 mt-5">
                            <div class="card addstf-form">
                                <header class="card-header">
                                    <h4 class="card-title mt-2">Create New Product</h4>
                                </header>
                                <article class="card-body">
                                    <form action="" method="POST" enctype="multipart/form-data">
                                        <div class="form-row d-flex flex-wrap justify-content-around ">
                                            <div class="col-4">
                                                <label>Product name </label>
                                                <input type="text" class="form-control ad" placeholder="" required
                                                    name="p_name" value="" minlength="4" maxlength="25">
                                            </div>
                                            <!-- form-group end.// -->
                                            <div class="col-4">
                                                <label>Product Category</label>
                                                <!-- <select id="inputState" class="form-control"> -->
                                                <select id="p_cate" name="p_cate" class="form-control ml-0" required>
                                                    <option value="Lanna">Lanna Mask</option>
                                                    <option value="Giant">Giant Mask</option>
                                                    <option value="Special">Special title treatment</option>
                                                </select>
                                                <!-- </select> -->
                                            </div>
                                            <!-- form-group end.// -->
                                        </div>
                                        <div class="form-row d-flex flex-wrap justify-content-around">
                                            <div class="col-4">
                                                <label>Model Year</label>
                                                <input type="text" class="form-control ad" placeholder="xxxx"
                                                    required name="p_mdy" value="" minlength="4" maxlength="15">
                                            </div>
                                            <!-- form-group end.// -->
                                            <div class="col-4">
                                                <label>Product Detail</label>
                                                <input type="text" class="form-control ad" placeholder="" required
                                                    name="p_det" value="" minlength="4" maxlength="150">
                                                <!-- <small class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                            </div>
                                            <!-- form-group end.// -->
                                        </div>
                                        <!-- form-group end.// -->
                                        <div class="form-row d-flex flex-wrap justify-content-around">
                                            <div class="col-4">
                                                <label>Price</label>
                                                <input 
                                                    type="text" 
                                                    class="form-control ad"
                                                    placeholder=""
                                                    required
                                                    name="p_price" 
                                                    value="" 
                                                    minlength="2" 
                                                    maxlength="50">
                                            </div>
                                            <div class="col-4">
                                                <label>Discount price</label>
                                                <input 
                                                    type="text" 
                                                    class="form-control ad"                                                     
                                                    name="p_pdic"
                                                    value="" 
                                                    minlength="1"
                                                    maxlength="15">
                                                  
                                            </div>
                                            <!-- form-group end.// -->

                                            <!-- form-group end.// -->
                                        </div>
                                        <!-- form-row.// -->
                                        <div class="form-row d-flex flex-wrap justify-content-around">
                                            
                                            <div class="col-4">
                                                <label>Promotion</label>
                                                <input class="form-control ad" type="text" name="p_pmt" placeholder="xx %"
                                                    value="" minlength="2" maxlength="20">
                                            </div>
                                            <div class="col-4">
                                                <label>Quantity</label>
                                                <input class="form-control ad" placeholder="Enter 1-100" type="number" 
                                                    required name="p_qtt" value="" minlength="1" maxlength="50">
                                            </div>
                                        </div>
                                        <div class="form-row d-flex flex-wrap justify-content-around">
                                            <div class="col-10">
                                                <label>Product image</label>
                                                <input class="form-control ad" type="file" required name="txt_file"
                                                 value="" minlength="4" maxlength="250">
                                            </div>
                                            
                                        </div>
                                        <div class="form-group mt-5">
                                            <div class="col-sm-12 text-center mt-5">
                                                <input type="submit" name="btn_insert" class="col-5 btn btn-success" value="Insert">
                                                <button class="col-5 btn btn-danger " type="reset">Reset</button>
                                            </div>
                                        </div>
                                        <!-- form-group// -->
                                        <small class="text-muted" style="margin-left: 4rem;">
                                            By clicking the 'Insert' button, you confirm that product information of
                                            use .
                                        </small>
                                    </form>
                                </article>
                                <!-- card-body end .// -->


                    </div> <!-- row.//-->


                </div>

            </main>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"
        integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"
        integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha"
        crossorigin="anonymous"></script>
    <script src="dashboard.js"></script>

</body>

</html>


    


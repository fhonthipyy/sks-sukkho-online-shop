
<!-- Admin  -->
<?php
    session_start();

        $db_host = "localhost";
        $db_user = "root";
        $db_password = "";
        $db_name = "mydb";
    
        try{
            $db = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_password);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            $e->getMessage();
        }
        

        if(isset($_REQUEST['delete_id'])){
            $id = $_REQUEST['delete_id'];

            $select_stmt = $db->prepare('SELECT * FROM products WHERE p_id = :id');
            $select_stmt->bindParam(':id', $id);
            $select_stmt->execute();
            $row = $select_stmt->fetch(PDO::FETCH_ASSOC);
            unlink("upload/".$row['p_img']); //remove img out off folder upload
           
            //delete an original record
            $delete_stmt = $db->prepare('DELETE FROM products WHERE p_id = :id');
            $delete_stmt->bindParam(':id', $id);
            $delete_stmt->execute();
            
            header("Location:product_mng.php");

        }
?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>SKS Administrate</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/dashboard/">

    <!-- Custom styles for this template -->
    <link href="../staff/staff.css" rel="stylesheet">
    <script src="admin-homepage.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"></script>
<!---------- function alert ------------->
        <script language="JavaScript" type="text/javascript">
            function checkDelete(){
                return confirm('Are you sure you want to delete this item?');
            }
        </script>
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
</head>

<body>

    <header class="navbar navbar-dark sticky-top bg-light flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" style="background-color: white;" href="../admin-homepage/admin-homepage.php">
            <img class="logo-shop" style="width: 200px;" src="../img/logo.png" alt="logo">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                <a href="#" class="btn btn-primary" style="font-size:16px;"><?php echo $_SESSION['uname'];?></a>
                <a class=""  href="../_logout.php" onclick="return confirm('Are you sure you want to sign out?')" >
                   <button class="btn btn-dark" style="margin-right:2rem;" >Sign out</button> 
                </a>
            </div>
        </div>
    </header>

    <div class="container-fluid">
        <div class="row">
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="../admin-homepage/admin-homepage.php">
                                <span data-feather="home">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-home" aria-hidden="true">
                                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                        <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                    </svg>
                                </span>
                                Administrate
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../admin-homepage/a-order-mng.php">
                                <span data-feather="file">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-file" aria-hidden="true">
                                        <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>
                                        <polyline points="13 2 13 9 20 9"></polyline>
                                    </svg>
                                </span>
                                Orders
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" aria-current="disable" href="#">
                                <span data-feather="shopping-cart">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-shopping-cart"
                                        aria-hidden="true">
                                        <circle cx="9" cy="21" r="1"></circle>
                                        <circle cx="20" cy="21" r="1"></circle>
                                        <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6">
                                        </path>
                                    </svg>
                                </span>
                                Products
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../admin-homepage/a_cus_mng.php">
                                <span data-feather="users">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-users" aria-hidden="true">
                                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="9" cy="7" r="4"></circle>
                                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                    </svg>
                                </span>
                                Customers
                            </a>
                        </li>
                   

                    </ul>

                   
                </div>
            </nav>

            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="chartjs-size-monitor">
                    <div class="chartjs-size-monitor-expand">
                        <div class=""></div>
                    </div>
                    <div class="chartjs-size-monitor-shrink">
                        <div class=""></div>
                    </div>
                </div>
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Product Management</h1>
                 
                </div>
                
                <!-- <h2 style="margin: 3rem 0;">Product Management</h2> -->
                <div class="">
                        <a href="add_product.php">
                            <button class="btn btn-success btn-addstf">   
                                Add product
                            </button> 
                        </a>
                    </div>                
                
                <!-- <p>A = Admin &nbsp;&nbsp;&nbsp; O = Owner </p> -->

                <div class="b-example-divider"></div>
<!------------------------------- tab menu ------------------------------------------------>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item"  role="presentation">
                   
                       <button class="nav-link active"
                        id="lanna" 
                        data-bs-toggle="tab" 
                        data-bs-target="#nav-lanna" 
                        type="button" 
                        role="tab" 
                        aria-controls="lanna" 
                        aria-selected="true">
                        Lanna Mask
                        </button>
            
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" 
                        id="giant" 
                        data-bs-toggle="tab" 
                        data-bs-target="#nav-giant" 
                        type="button" 
                        role="tab"
                        aria-controls="giant"
                        aria-selected="false">Giant Mask
                    </button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" 
                      id="special"
                      data-bs-toggle="tab"
                      data-bs-target="#nav-special" 
                      type="button" 
                      role="tab" 
                      aria-controls="special"
                      aria-selected="false">Special title treatment
                    </button>
                </li>
            </ul>
     <!-------------------------------------- content product ------------------------------------------------>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="nav-lanna" role="tabpanel" aria-labelledby="lanna">
                        <br><br>
                <div class="table-responsive">
                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Model</th>
                                <th scope="col">Detail</th>
                                <th scope="col">Price</th>
                                <th scope="col">Discount</th>
                                <th scope="col">Promotion</th>
                                <th scope="col">Image</th>
                                <th scope="col">Quantity</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $select_stmt = $db->prepare('SELECT * FROM products WHERE(p_cate="Lanna")');
                            $select_stmt->execute();

                            while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)){                
                        ?>
                                        
                                <td><?php echo $row['p_name']; ?></td>
                                <td><?php echo $row['p_mdy']; ?></td>
                                <td><?php echo $row['p_det']; ?></td>
                                <td><?php echo $row['p_price']; ?></td>
                                <td><?php echo $row['p_pdic']; ?></td>
                                <td><?php echo $row['p_pmt']; ?></td>
                                <td><img src="upload/<?php echo $row['p_img']; ?>" width="100px" height="100px" alt=""></td>
                                <td><?php echo $row['p_qtt']; ?></td>

                                <td><a href="edit_product.php?update_id=<?php echo $row['p_id']; ?>" class="btn btn-warning">Edit</a></td>
                                <td><a href="?delete_id=<?php echo $row['p_id']; ?>" class="btn btn-danger" onclick="return checkDelete()">Delete</a></td>
                            </tr>

                                
                        <?php } ?>
                             
                        </tbody>  
                    </table>
                </div> 

                </div>
                <div class="tab-pane fade" id="nav-giant" role="tabpanel" aria-labelledby="giant">
                <br><br>
                <div class="table-responsive">
                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Model</th>
                                <th scope="col">Detail</th>
                                <th scope="col">Price</th>
                                <th scope="col">Discount</th>
                                <th scope="col">Promotion</th>
                                <th scope="col">Image</th>
                                <th scope="col">Quantity</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php
                            $select_stmt = $db->prepare('SELECT * FROM products  WHERE(p_cate="Giant")');
                            $select_stmt->execute();

                            while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)){                
                        ?>
                                <td><?php echo $row['p_name']; ?></td>
                                <td><?php echo $row['p_mdy']; ?></td>
                                <td><?php echo $row['p_det']; ?></td>
                                <td><?php echo $row['p_price']; ?></td>
                                <td><?php echo $row['p_pdic']; ?></td>
                                <td><?php echo $row['p_pmt']; ?></td>
                                <td><img src="upload/<?php echo $row['p_img']; ?>" width="100px" height="100px" alt=""></td>
                                <td><?php echo $row['p_qtt']; ?></td>

                                <td><a href="edit_product.php?update_id=<?php echo $row['p_id']; ?>" class="btn btn-warning">Edit</a></td>
                                <td><a href="?delete_id=<?php echo $row['p_id']; ?>" class="btn btn-danger" onclick="return checkDelete()" >Delete</a></td>
                            </tr>

                                
                        <?php } ?>
                            
                        </tbody>  
                    </table>
                </div> 
                </div>
                <div class="tab-pane fade" id="nav-special" role="tabpanel" aria-labelledby="special">
                <br><br>
                <div class="table-responsive">
                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Model</th>
                                <th scope="col">Detail</th>
                                <th scope="col">Price</th>
                                <th scope="col">Discount</th>
                                <th scope="col">Promotion</th>
                                <th scope="col">Img</th>
                                <th scope="col">Quantity</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $select_stmt = $db->prepare('SELECT * FROM products WHERE (p_cate="Special")');
                            $select_stmt->execute();

                            while ($row = $select_stmt->fetch(PDO::FETCH_ASSOC)){                
                        ?>
                                        <td><?php echo $row['p_name']; ?></td>
                                        <td><?php echo $row['p_mdy']; ?></td>
                                        <td><?php echo $row['p_det']; ?></td>
                                        <td><?php echo $row['p_price']; ?></td>
                                        <td><?php echo $row['p_pdic']; ?></td>
                                        <td><?php echo $row['p_pmt']; ?></td>
                                        <td><img src="upload/<?php echo $row['p_img']; ?>" width="100px" height="100px" alt=""></td>
                                        <td><?php echo $row['p_qtt']; ?></td>
        
                                        <td><a href="edit_product.php?update_id=<?php echo $row['p_id']; ?>" class="btn btn-warning">Edit</a></td>
                                        <td><a href="?delete_id=<?php echo $row['p_id']; ?>" class="btn btn-danger" onclick="return checkDelete()">Delete</a></td>
                                    </tr>
        
                                        
                                <?php } ?>
                            
                        </tbody>  
                    </table>
                </div> 
                </div>
            </div>


               
            </main>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"
        integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"
        integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha"
        crossorigin="anonymous"></script>
    <script src="dashboard.js"></script>


</body>

</html>
<?php
       session_start();
        //connection
        $db_host = "localhost";
        $db_user = "root";
        $db_password = "";
        $db_name = "mydb";
    
        try{
            $db = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_password);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            $e->getMessage();
        }
    

        if(isset($_REQUEST['detail_id'])){
            try{
                $id = $_REQUEST['detail_id'];
                $select_stmt = $db->prepare('SELECT order_detail.o_id,order_detail.qty,products.* FROM order_detail,products
                WHERE o_id =:id ');
                $select_stmt->bindParam(":id", $id);
                $select_stmt->execute();
                $row = $select_stmt->fetch(PDO::FETCH_ASSOC);
                //show old detail before edit
                extract($row);


            } catch(PDOException $e){
                $e->getMessage();
            }             
         }

                
    
    
    ?>


<!DOCTYPE html>
<html lang="en">

<head>
  <!-------------------------------------------------- Title name ------------------------------------------->
  <title>SKS Edit</title>

  <!-------------------------------------------------- setting ------------------------------------------->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-------------------------------------------------- style ------------------------------------------->
  <link rel="stylesheet" href="../register/register.css">
  <script src="https://kit.fontawesome.com/yourcode.js" crossorigin="anonymous"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
    crossorigin="anonymous"></script>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
</head>


<style>
    .dropbtn {
        background-color: #380B10;
        color: white;
        margin-right: 3rem;
        padding: 6px 10px;
        font-size: 20px;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        }

        .dropdown {
        position: relative;
        display: inline-block;
        background-color:transparent;
        }

        .dropdown-content {
        display: none;
        position: absolute;
        right: 3rem;
        background-color: #f9f9f9;
        min-width: 140px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        }

        .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        
        }

        .dropdown-content a:hover {background-color: #f1f1f1;}

        .dropdown:hover .dropdown-content {
        display: block;
        }

        .dropdown:hover .dropbtn {
        background-color:#A59193;
        color: black;
        }

        .w3-modal-content {
            width: 660px;
        }

        .text-center {
            text-align: center!important;
            background-color: #A59193;
            color: #fffff;10;
        }
        .w3-container, .w3-panel {
            padding: 0;
        }
        .w3-display-topright {
            position: absolute;
            right: 0;
            top: 0;
            color: white;
            z-index: 9;
        }
</style>
<body style="background-color: #d8d8d8;">
  
  
  <!-- --------------------------------------------- nav-bar------------------------------------------- -->

  <header class="navbar navbar-dark sticky-top bg-light flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" style="background-color: white;" href="./owner.php">
            <img class="logo-shop" style="width: 200px;" src="../img/logo.png" alt="logo">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                   
            <div class="dropdown " >
                <button class="dropbtn"><?php echo $_SESSION['uname'];?></button>
                <div class="dropdown-content">
                  
                    <a onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-white ">Profile</a>
                   
                    
                    <a class="w3-button w3-white" href="../_logout.php" onclick="return confirm('Are you sure you want to sign out?')" >
                         Sign out
                    </a>
                    
                </div>
            </div>

            
                
            </div>
        </div>
    </header>
    <!-- --------------------------------------------- edit form ------------------------------------------- -->  
  <center>
    <div class="container" style="margin-top: 5px;">
                    <div class="row d-flex justify-content-center ">
                    <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="nav-lanna" role="tabpanel" aria-labelledby="lanna">
                        <br><br>
                <div class="table-responsive">
                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Order No.</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Price</th>
                                <th scope="col">Product image</th>
                                <th scope="col">Quantity(pcs)</th>
                                <!-- <th scope="col"></th>
                                <th scope="col"></th> -->
                            </tr>
                        </thead>
                        <tbody>
                       
                                        
                                <td><?php echo $row['o_id']; ?></td>
                                <td><?php echo $row['p_name']; ?></td>
                                <td><?php echo $row['p_pdic']; ?></td>
                                <td><?php  echo "<img src='upload/" . $row["p_img"] ." 'width=50%; height=80px;' >"; ?></td>
                                <td><?php echo  "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ". $row['qty']; ?></td>
                                
<!--                               
                                <td><a href="edit_o.php?update_id=<?php echo $row['o_id']; ?>" class="btn btn-warning">Edit</a></td>
                                <td><a href="?delete_id=<?php echo $row['o_id']; ?>" class="btn btn-danger" onclick="return checkDelete()">Delete</a></td> -->
                            </tr>

                                
                        
                             
                        </tbody> 
                        
                        </table>
                </div> 

                    </div> <!-- row.//-->
                    <div class="col-2 form-group" style="margin-top: 2px;">
                            <a href="om_order.php"> <button class="btn-block btn btn-danger" type="button">Cancel</button></a>
                    </div> 

                </div>
                </center>


</body>

</html>


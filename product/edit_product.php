
<!-- Admin -->

<?php
session_start();
        //connection
        $db_host = "localhost";
        $db_user = "root";
        $db_password = "";
        $db_name = "mydb";
    
        try{
            $db = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_password);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            $e->getMessage();
        }
    

        if(isset($_REQUEST['update_id'])){
            try{
                $id = $_REQUEST['update_id'];
                $select_stmt = $db->prepare('SELECT * FROM products WHERE p_id = :id');
                $select_stmt->bindParam(":id", $id);
                $select_stmt->execute();
                $row = $select_stmt->fetch(PDO::FETCH_ASSOC);
                //show old detail before edit
                extract($row);
            } catch(PDOException $e){
                $e->getMessage();
            }             
         }

         if (isset($_REQUEST['btn_update'])){
            try{
                $p_name=$_REQUEST['p_name']; 
                $p_cate=$_REQUEST['p_cate'];                                 
                $p_mdy=$_REQUEST['p_mdy'];
                $p_det=$_REQUEST['p_det'];
                $p_price=$_REQUEST['p_price'];
                $p_pdic=$_REQUEST['p_pdic'];
                $p_pmt=$_REQUEST['p_pmt'];
    
                $image_file = $_FILES['txt_file']['name'];
                $type = $_FILES['txt_file']['type'];
                $size = $_FILES['txt_file']['size'];
                $temp = $_FILES['txt_file']['tmp_name'];
    
                $p_qtt=$_REQUEST['p_qtt'];

                $path = "/product/upload/" . $image_file; //set upload folder path
                // echo $image_file;
                // exit;
                $directory = "upload/";

                if ($image_file){
                    if ($type  == "image/jpg" || $type == 'image/jpeg' || $type == 'image/png' || $type == 'image/gif'){
                        if (!file_exists($path)){ //check path
                            if($size < 5000000){
                                unlink($directory.$row['p_img']); //remove old file
                                move_uploaded_file($temp, 'upload/'.$image_file); //update new file to folder
                            }else{
                                $errorMsg = "Your File too large pls upload 5MB size";
                            }
                        }else{
                            $errorMsg = "File already Exists.. Check upload folder ";
                        }
                    }else{
                        $errorMsg = "Upload JPG,JPEG,PNG & GIF formats....";
                    }
                } else {
                    $image_file = $row['p_img'];
                }
    
                if(!isset($errorMsg)){
                    $update_stmt = $db->prepare('UPDATE products 
                        SET p_name = :p_name,
                            p_cate = :p_cate,
                            p_mdy = :p_mdy,
                            p_det = :p_det,
                            p_price = :p_price,
                            p_pdic = :p_pdic,
                            p_pmt = :p_pmt,
                            p_img = :p_img,
                            p_qtt = :p_qtt
                        WHERE p_id = :id');

  
                    $update_stmt->bindParam('p_name',$p_name);
                    $update_stmt->bindParam('p_cate', $p_cate);
                    $update_stmt->bindParam('p_mdy', $p_mdy);
                    $update_stmt->bindParam('p_det', $p_det);
                    $update_stmt->bindParam('p_price', $p_price);
                    $update_stmt->bindParam('p_pdic', $p_pdic);
                    $update_stmt->bindParam('p_pmt', $p_pmt);
                    $update_stmt->bindParam('p_img',$image_file);
                    $update_stmt->bindParam('p_qtt', $p_qtt);
                    $update_stmt->bindParam('id',$id);
   
                    if ($update_stmt->execute()){
                        $updateMsg = "File Update Successfully...";
                        header('refresh:1;product_mng.php');
                    }
                }
    
            } catch(PDOException $e){
                $e->getMessage();
            }
        }
    
    
    ?>


<!DOCTYPE html>
<html lang="en">

<head>
  <!-------------------------------------------------- Title name ------------------------------------------->
  <title>SKS Product Edit</title>

  <!-------------------------------------------------- setting ------------------------------------------->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-------------------------------------------------- style ------------------------------------------->
  <link rel="stylesheet" href="add_product.css">
  <script src="https://kit.fontawesome.com/yourcode.js" crossorigin="anonymous"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
    crossorigin="anonymous"></script>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
</head>

<style>
    header.card-header {
    background-color: #A59193;
    color:black;
    border-radius:10px 10px 0 0!important;

  }

     .dropbtn {
        background-color: #380B10;
        color: white;
        margin-right: 3rem;
        padding: 6px 10px;
        font-size: 20px;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        }

        .dropdown {
        position: relative;
        display: inline-block;
        background-color:transparent;
        }

        .dropdown-content {
        display: none;
        position: absolute;
        right: 3rem;
        background-color: #f9f9f9;
        min-width: 140px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        }

        .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        
        }

        .dropdown-content a:hover {background-color: #f1f1f1;}

        .dropdown:hover .dropdown-content {
        display: block;
        }

        .dropdown:hover .dropbtn {
        background-color:#A59193;
        color: black;
        }

        .w3-modal-content {
            width: 660px;
        }

        .text-center {
            text-align: center!important;
            background-color: white;
            color: #fffff;10;
        }
        .w3-container, .w3-panel {
            padding: 0;
        }
        .w3-display-topright {
            position: absolute;
            right: 0;
            top: 0;
            color: white;
            z-index: 9;
        }

</style>

<body style="background-color: #d8d8d8;">
  
  
  
  <!-- --------------------------------------------- nav-bar------------------------------------------- -->

  <header class="navbar navbar-dark sticky-top bg-light flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" style="background-color: white;" href="../admin-homepage/admin-homepage.php">
            <img class="logo-shop" style="width: 200px;" src="../img/logo.png" alt="logo">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                   
            <div class="dropdown " >
                <button class="dropbtn">Hi! <?php echo $_SESSION['uname'];?></button>
                <div class="dropdown-content">
                  
                    <a onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-white ">Profile</a>
                   
                    
                    <a class="w3-button w3-white" href="../_logout.php" onclick="return confirm('Are you sure you want to sign out?')" >
                         Sign out
                    </a>
                    
                </div>
            </div>

            
                
            </div>
        </div>
    </header>
   <!-- --------------------------------------------- edit form ------------------------------------------- -->  
   <div class="container" style="margin-top: 5px;">
                    <div class="row justify-content-center ">
                           <!-- shhow error message -->
                        <?php 
                            if (isset($errorMsg)){ 
                        ?>
                            <div class="alert alert-danger text-center">
                                <strong><?php echo $errorMsg; ?></strong>
                            </div>
                        <?php } ?>
                    <!-- shhow success message -->
                        <?php 
                            if (isset($updateMsg)){ 
                        ?>
                            <div class="alert alert-success text-center">
                                <strong><?php echo $updateMsg; ?></strong>
                            </div>
                        <?php } ?>

                        <div class="col-md-8 pt-0 ">
                            <div class="card addstf-form">
                                <header class="card-header pt-1 pb-0">
                                    <h4 class="card-title mt-1">Edit Product</h4>
                                </header>
                                <article class="card-body pb-0">
                                    <form action=""  method="POST" class="form-horizontal" enctype="multipart/form-data">
                                        <div class="form-row d-flex flex-wrap justify-content-around ">
                                            <div class="col-4">
                                                <label>Product name </label>
                                                <input type="text" class="form-control ad" placeholder="" required
                                                    name="p_name" value="<?php echo $p_name; ?>" minlength="4" maxlength="15">
                                            </div>
                                            <!-- form-group end.// -->
                                            <div class="col-4">
                                                <label>Product Category</label>
                                                <!-- <select id="inputState" class="form-control"> -->
                                                <select id="p_cate" name="p_cate" class="form-control ml-0" required>
                                                    <option ><?php echo $p_cate; ?></option>
                                                    <option value="Lanna">Lanna Mask</option>
                                                    <option value="Giant">Giant Mask</option>
                                                    <option value="Special">Special title treatment</option>
                                                </select>
                                                <!-- </select> -->
                                            </div>
                                            <!-- form-group end.// -->
                                        </div>
                                        <div class="form-row d-flex flex-wrap justify-content-around">
                                            <div class="col-4">
                                                <label>Model Year</label>
                                                <input type="text" class="form-control ad" placeholder="xxxx"
                                                    required name="p_mdy" value="<?php echo $p_mdy; ?>" minlength="4" maxlength="15">
                                            </div>
                                            <!-- form-group end.// -->
                                            <div class="col-4">
                                                <label>Product Detail</label>
                                                <input type="text" class="form-control ad" placeholder="" required
                                                    name="p_det" value="<?php echo $p_det; ?>" minlength="4" maxlength="150">
                                                <!-- <small class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                            </div>
                                            <!-- form-group end.// -->
                                        </div>
                                        <!-- form-group end.// -->
                                        <div class="form-row d-flex flex-wrap justify-content-around">
                                            <div class="col-4">
                                                <label>Price</label>
                                                <input type="text" class="form-control ad" placeholder="" required
                                                    name="p_price" value="<?php echo $p_price; ?>" minlength="2" maxlength="50">
                                            </div>
                                            <div class="col-4">
                                                <label>Discount price</label>
                                                <input type="text" class="form-control ad"  name="p_pdic"
                                                 value="<?php echo $p_pdic; ?>" minlength="2" maxlength="15">
                                            </div>
                                            <!-- form-group end.// -->
                                        </div>
                                        <!-- form-row.// -->
                                        <div class="form-row d-flex flex-wrap justify-content-around">
                                            
                                            <div class="col-4">
                                                <label>Promotion</label>
                                                <input class="form-control ad" type="text"  name="p_pmt" placeholder="xx %"
                                                    value="<?php echo $p_pmt; ?>" minlength="2" maxlength="20">
                                            </div>
                                            <div class="col-4">
                                                <label>Quantity</label>
                                                <input class="form-control ad" placeholder="Enter 1-100" type="text"
                                                    required name="p_qtt" value="<?php echo $p_qtt; ?>" minlength="1" maxlength="50">
                                            </div>
                                        </div>
                                        <div class="form-row d-flex flex-wrap justify-content-around">
                                            <div class="col-10">
                                                <label>Product image</label>
                                                    <input type="file" name="txt_file" class="form-control" value="<?php echo $image_file; ?>">
                                                <p class="text-center mt-2">
                                                    <img src="upload/<?php echo $row['p_img']; ?>" height="150px" width="150px" alt="">
                                                </p>
                                            </div>
                                            
                                        </div>
                                        <div class="form-row d-flex flex-wrap justify-content-around">
                                        <div class="col-6 form-group" style="margin-top: 2px;">
                                            <input class="btn-block btn btn-success mb-1" name="btn_update" id="btn-confupdate" type="submit" onclick="alert('Update product successful !!')" value="Update">
                                        </div>
                                        <div class="col-6 form-group" style="margin-top: 2px;">
                                            <a href="product_mng.php"> <button class="btn-block btn btn-danger" type="button">Cancel</button></a>
                                        </div>
                                        </div>
                                    </form>
                                </article>
                                <!-- card-body end .// -->

                            </div>
                            <!-- card.// -->
                        </div> <!-- col.//-->

                    </div> <!-- row.//-->


                </div>
  <!--container end.//-->


</body>

</html>


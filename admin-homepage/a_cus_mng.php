<?php
        //connection
        $con= mysqli_connect("localhost","root","","mydb") or die("Error: " . mysqli_error($con));
        mysqli_query($con, "SET NAMES 'utf8' "); 
?>

<?php 
    session_start();

    if($_SESSION["urole"]!="A"){
        header('Location:../index.php');
        exit();
    }
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>SKS Administrate</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/dashboard/">

    <!-- Custom styles for this template -->
    <link href="admin-homepage.css" rel="stylesheet">
    <script src="admin-homepage.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"></script>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .dropbtn {
        background-color: #380B10;
        color: white;
        margin-right: 3rem;
        padding: 6px 10px;
        font-size: 20px;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        }

        .dropdown {
        position: relative;
        display: inline-block;
        background-color:transparent;
        }

        .dropdown-content {
        display: none;
        position: absolute;
        right: 3rem;
        background-color: #f9f9f9;
        min-width: 140px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        }

        .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        
        }

        .dropdown-content a:hover {background-color: #f1f1f1;}

        .dropdown:hover .dropdown-content {
        display: block;
        }

        .dropdown:hover .dropbtn {
        background-color:#A59193;
        color: black;
        }

        .w3-modal-content {
            width: 660px;
        }

        .text-center {
            text-align: center!important;
            background-color: #A59193;
            color: #fffff;10;
        }
        .w3-container, .w3-panel {
            padding: 0;
        }
        .w3-display-topright {
            position: absolute;
            right: 0;
            top: 0;
            color: white;
            z-index: 9;
        }


    </style>



</head>

<body>
<header class="navbar navbar-dark sticky-top bg-light flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" style="background-color: white;" href="./admin-homepage.php">
            <img class="logo-shop" style="width: 200px;" src="../img/logo.png" alt="logo">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                   
            <div class="dropdown " >
                <button class="dropbtn"><?php echo $_SESSION['uname'];?></button>
                <div class="dropdown-content">
                  
                    <a onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-white ">Profile</a>
                   
                    
                    <a class="w3-button w3-white" href="../_logout.php" onclick="return confirm('Are you sure you want to sign out?')" >
                         Sign out
                    </a>
                    
                </div>
            </div>           
                
            </div>
        </div>
    </header>

    <div class="container-fluid">
        <div class="row">
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="./admin-homepage.php">
                                <span data-feather="home">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-home" aria-hidden="true">
                                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                        <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                    </svg>
                                </span>
                                Administrate
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="./a-order-mng.php">
                                <span data-feather="file">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-file" aria-hidden="true">
                                        <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>
                                        <polyline points="13 2 13 9 20 9"></polyline>
                                    </svg>
                                </span>
                                Orders
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../product/product_mng.php">
                                <span data-feather="shopping-cart">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-shopping-cart"
                                        aria-hidden="true">
                                        <circle cx="9" cy="21" r="1"></circle>
                                        <circle cx="20" cy="21" r="1"></circle>
                                        <path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6">
                                        </path>
                                    </svg>
                                </span>
                                Products
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" aria-current="disable" href="#">
                                <span data-feather="users">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" class="feather feather-users" aria-hidden="true">
                                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="9" cy="7" r="4"></circle>
                                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                    </svg>
                                </span>
                                Customers
                            </a>
                        </li>
                   

                    </ul>

                   
                </div>
            </nav>

            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="chartjs-size-monitor">
                    <div class="chartjs-size-monitor-expand">
                        <div class=""></div>
                    </div>
                    <div class="chartjs-size-monitor-shrink">
                        <div class=""></div>
                    </div>
                </div>
                <div
                    class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Customer Management</h1>
                 
                </div>
                
                <!-- <h2 style="margin: 3rem 0;">Customer Management</h2> -->
                <div class="table-responsive">
                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <!-- <th scope="col">id</th> -->
                                <th scope="col">Firstname</th>
                                <th scope="col">Lastname</th>
                                <th scope="col">Phone</th>
                                <th scope="col">E-mail</th>
                                <th scope="col">Address</th>
                                <th scope="col">Country</th>
                                <th scope="col">Zipcode</th>
                                <th scope="col">Username</th>
                                <th scope="col">Password</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php

                                $sql1="SELECT * FROM users WHERE(urole='M')";
                                $rst=mysqli_query($con,$sql1);
                                if($rst){                                    
                                     while($rows=mysqli_fetch_assoc($rst)){
                                        $id=$rows['id'];
                                        $fname=$rows['fname'];
                                        $lname=$rows['lname'];
                                        $phone=$rows['phone'];
                                        $email=$rows['email'];
                                        $addr=$rows['addr'];
                                        $country=$rows['country'];
                                        $zip=$rows['zip'];
                                        $uname=$rows['uname'];
                                        $psw=$rows['psw'];
                                        echo ' <tr>
                                    
                                        <td>'.$fname.'</td>
                                        <td>'.$lname.'</td>
                                        <td>'.$phone.'</td>
                                        <td>'.$email.'</td>
                                        <td>'.$addr.'</td>
                                        <td>'.$country.'</td>
                                        <td>'.$zip.'</td>
                                        <td>'.$uname.'</td>
                                        <td>'.$psw.'</td>
                                        <td>
                                            <button class="btn btn-warning">
                                                <a href="../edit/edit.php? updateid='.$id.'"  class="text-light" style="text-decoration:none;">Edit</a>
                                            </button> &nbsp;&nbsp;
                                            <button class="btn btn-danger">
                                                <a href="../delete/delete.php? deleteid='.$id.'" onclick="confirm_entry()" class="text-light" style="text-decoration:none;">Delete</a>
                                            </button>
                                        </td>

                                        </tr>' ;
                                    }
                                }
                    
                            ?>
                            
                        </tbody>  
                    </table>
                </div>
            </main>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"
        integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"
        integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha"
        crossorigin="anonymous"></script>
    <script src="dashboard.js"></script>

    <!-- ------------- delete message alert-------------------- -->
    <script language="JavaScript">
        function confirm_entry()
        {
            input_box=confirm("Click OK or Cancel to Continue");
            
            if (input_box==true)
            {
                alert ("You clicked OK");
            }
            else
            {
                alert ("You clicked cancel");
            }
        }

    </script>

</body>

</html>